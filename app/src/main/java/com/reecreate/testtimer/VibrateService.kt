package com.reecreate.testtimer

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.os.Vibrator

/**
 * Created by reecreate on 15/03/2018.
 */
class VibrateService : Service() {

    override fun onStart(intent: Intent, startId: Int) {
        super.onStart(intent, startId)

        val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        // pass the number of milliseconds to vibrate the phone
        // if want to vibrate in a pattern
        // long pattern[] = {0, 800, 200, 800}
        // Second argument is for repeition pass -1 if do not want to repeat.
        // v.vibrate(pattern, -1);
        v.vibrate(1000)

        // Point to note:
        // first 0 means silent for 0 millis
        // 800 means vibrate for 800 millis
        // 200 means silent for 200 millis
        // 800 means vibrate for 800 millis
        // So on. 0 and Even index means silent. Odd = vibrate


    }



    override fun onBind(intent: Intent): IBinder? {
        return null
    }

}
