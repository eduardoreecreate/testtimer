package com.reecreate.testtimer

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.preference.PreferenceActivity
import android.preference.PreferenceFragment
import com.reecreate.testtimer.SettingsActivity.MyPreferenceFragment





class SettingsActivity : PreferenceActivity() {
  //  private var sendingMail = false
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentManager.beginTransaction().replace(android.R.id.content, MyPreferenceFragment()).commit()


    }

    class MyPreferenceFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.preferences)
        }
    }


    override fun onBackPressed() {
        val mainActivity = Intent(this, MainActivity::class.java)
        mainActivity.addCategory(Intent.CATEGORY_HOME)
        mainActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(mainActivity)
        finish()
    }


//    override fun onResume() {
//        sendingMail = false
//        super.onResume()
//    }

//    override fun onStop() {
//        if (!sendingMail) {
//            val i = Intent(this, MainActivity::class.java)
//            startActivity(i)
//        }
//        super.onStop()
//    }




}