package com.reecreate.testtimer

import android.content.Context
import android.content.res.TypedArray
import android.os.Build
import android.preference.DialogPreference
import android.util.AttributeSet
import android.view.View
import android.widget.TimePicker
import java.sql.Time

/**
 * Created by reecreate on 15/03/2018.
 */
class TimePreference(context: Context, attributeSet: AttributeSet) : DialogPreference(context, attributeSet) {

    private var lastMinute = 0
    private var lastSecond = 0
    private var picker: TimePicker? = null

    init {

        setPositiveButtonText(R.string.set)
        setNegativeButtonText(R.string.cancel)
    }

    override fun onCreateDialogView(): View {
        picker = TimePicker(context)
        picker!!.setIs24HourView(true)
        return picker as TimePicker
    }

    override fun onBindDialogView(view: View) {
        super.onBindDialogView(view)

        if(Build.VERSION.SDK_INT >= 23) {

            picker!!.hour = lastMinute
            picker!!.minute = lastSecond
        }
    }

    override fun onDialogClosed(positiveResult: Boolean) {
        super.onDialogClosed(positiveResult)

        if (positiveResult) {
            if(Build.VERSION.SDK_INT >= 23) {
                lastMinute = picker!!.hour
                lastSecond = picker!!.minute
            }

            val time = lastMinute.toString() + ":" + lastSecond.toString()

            if (callChangeListener(time)) {
                persistString(time)
            }
            super.setSummary(summary)
        }
    }

    override fun onGetDefaultValue(a: TypedArray, index: Int): Any? {
        return a.getString(index)
    }

    override fun onSetInitialValue(restoreValue: Boolean, defaultValue: Any?) {
        var time: String? = null
        if (restoreValue) {
            if (defaultValue == null) {
                time = getPersistedString("00:00")
            } else {
                time = getPersistedString(defaultValue.toString())
            }
        } else {
            time = defaultValue!!.toString()
        }

        lastMinute = getMinute(time)
        lastSecond = getSecond(time)
    }



    override fun getSummary(): CharSequence {
        val time = Time(0, lastMinute, lastSecond)


        return String.format("%02d:%02d", time.minutes, time.seconds)

    }

    companion object {

        fun getMinute(time: String?): Int {
            val pieces = time!!.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            return Integer.parseInt(pieces[0])
        }

        fun getSecond(time: String?): Int {
            val pieces = time!!.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            return Integer.parseInt(pieces[1])
        }

        fun getMillis(time: String): Long {
            val pieces = time.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            return Integer.parseInt(pieces[0]).toLong() * 60 * 1000 + Integer.parseInt(pieces[1]).toLong() * 1000
        }
    }
}

